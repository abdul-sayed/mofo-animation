package com.sayed.mobileforumanimation

import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.constraintlayout.motion.widget.MotionScene
import androidx.core.content.ContentProviderCompat.requireContext

class MainActivity : AppCompatActivity() {

    private lateinit var mediaPlayer: MediaPlayer
    private lateinit var motionLayout: MotionLayout

    enum class Scene {
        COUNTDOWN,
        LIFTOFF
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        motionLayout = findViewById(R.id.motionLayout)

        playScene(Scene.COUNTDOWN)
    }

    override fun onPause() {
        super.onPause()

        mediaPlayer.pause()
    }

    private fun playScene(scene: Scene) {
        when (scene) {
            Scene.COUNTDOWN -> {
                motionLayout.setTransition(R.id.transition_smoke)
                playSmokeAnimation()
                playCountdown()
            }

            Scene.LIFTOFF -> {
                motionLayout.setTransition(R.id.transition_liftoff)
                playLiftoffAnimation()
            }
        }
    }

    //region ANIMATIONS
    private fun playSmokeAnimation() {
        motionLayout.transitionToEnd {
            motionLayout.transitionToStart()
            playSmokeAnimation()
        }
    }

    private fun playLiftoffAnimation() {
        motionLayout.transitionToEnd()
    }
    //endregion

    //region AUDIO
    private fun playCountdown() {
        playAudio(R.raw.countdown.getUri()) {
            playLiftoff()
            playScene(Scene.LIFTOFF)
        }
    }

    private fun playLiftoff() {
        playAudio(R.raw.liftoff.getUri()) {
            playRocketLiftoff()
        }
    }

    private fun playRocketLiftoff() {
        playAudio(R.raw.rocket_liftoff.getUri())
    }

    private fun playAudio(uri: Uri, onComplete: (() -> Unit)? = null) {
        if (!::mediaPlayer.isInitialized) {
            mediaPlayer = MediaPlayer()
        }

        mediaPlayer.setDataSource(applicationContext, uri)
        mediaPlayer.setAudioAttributes(AudioAttributes.Builder().setLegacyStreamType(AudioManager.STREAM_MUSIC).build())
        mediaPlayer.prepare()
        mediaPlayer.start()
        mediaPlayer.setOnCompletionListener {
            it.reset()
            onComplete?.invoke()
        }
    }
    //endregion

    //region EXTENSIONS
    private fun Int.getUri() =
        Uri.parse("android.resource://$packageName/$this")
    //endregion
}